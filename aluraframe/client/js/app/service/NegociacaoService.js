class NegociacaoService {

  constructor() {
    this._http = new HttpService();
  }

  obterNegociacoesDaSemana() {
    
    let promise = new Promise((resolve, reject) => {
      this._http
        .get("negociacoes/semana")
        .then(negociacoes => {
          resolve(negociacoes.map(objeto => new Negociacao(new Date(objeto.data), objeto.quantidade, objeto.valor)));
        })
        .catch(erro => {
          reject('Não foi possível obter as negociações da semana');
        });
    });
    
    return promise;
  }

  obterNegociacoesDaSemanaAnterior() {
    
    let promise = new Promise((resolve, reject) => {
      this._http
        .get("negociacoes/anterior")
        .then(negociacoes => {
          resolve(negociacoes.map(objeto => new Negociacao(new Date(objeto.data), objeto.quantidade, objeto.valor)));
        })
        .catch(erro => {
          reject('Não foi possível obter as negociações da semana anterior');
        });
    });
    
    return promise;
  }

  obterNegociacoesDaSemanaRetrasada() {
    
    let promise = new Promise((resolve, reject) => {
      this._http
        .get("negociacoes/retrasada")
        .then(negociacoes => {
          resolve(negociacoes.map(objeto => new Negociacao(new Date(objeto.data), objeto.quantidade, objeto.valor)));
        })
        .catch(erro => {
          reject('Não foi possível obter as negociações da semana retrasada');
        });
    });
    
    return promise;
  }
}